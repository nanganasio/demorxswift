//
//  Model.swift
//  SampleRxSwift
//
//  Created by Adrian Cervantes on 5/3/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import Foundation

struct Model: Decodable {
    var name: String?
    var imageUrl: String?
}
