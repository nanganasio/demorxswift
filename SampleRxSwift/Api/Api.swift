//
//  Api.swift
//  SampleRxSwift
//
//  Created by Adrian Cervantes on 5/3/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import Foundation

// pending using generic value
class Api {
   static func getRequest<Type>(completion: @escaping (Result<Type, Error>) -> Void) {
        let urlString = "https://api.letsbuildthatapp.com/jsondecodable/courses"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
            }
            guard let data = data as? Type else {return }
            completion(.success(data))
        }.resume()
    }
}
