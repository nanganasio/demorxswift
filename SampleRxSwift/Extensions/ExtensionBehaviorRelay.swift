//
//  ExtensionBehaviorRelay.swift
//  SampleRxSwift
//
//  Created by Adrian Cervantes on 5/3/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import RxSwift
import RxCocoa

extension BehaviorRelay where Element: RangeReplaceableCollection {
    func acceptAppending(element: Element.Element) {
        accept(value + [element])
    }
    
    func acceptAppendingIndex(element: Element.Element, index: Element.Index) {
        var newValue = value
        newValue.insert(element, at: index)
        accept(newValue)
    }
    
    func remove(index: Element.Index) {
        var removeValue = value
        removeValue.remove(at: index)
        accept(removeValue)
    }
}
