//
//  CustomTextField.swift
//  SampleRxSwift
//
//  Created by Adrian Cervantes on 4/26/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import UIKit

class CustomTextField: UIView {
    var completionText: ((String) -> Void)?
    var placeHolder: String?
    
    lazy var imageIcon: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.backgroundColor = .clear
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.layer.cornerRadius = 7
        textField.placeholder = placeHolder
        return textField
    }()
    
    lazy var line: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .white
        setupViews()
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupValues(placeholder: String?, image: UIImage?) {
        textField.placeholder = placeholder
        imageIcon.image = image?.withRenderingMode(.alwaysTemplate)
        imageIcon.tintColor = .red
    }
    
    private func setupViews() {
        addSubview(textField)
        addSubview(line)
        addSubview(imageIcon)
        
        imageIcon.anchor(top: nil, leading: leadingAnchor,
                         bottom: nil, trailing: nil,
                         padding: .init(top: 0, left: 20, bottom: 0, right: 0),
                         size: CGSize(width: 25, height: 25))
        imageIcon.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        textField.delegate = self
        textField.anchor(top: nil, leading: imageIcon.trailingAnchor, bottom: nil, trailing: trailingAnchor, padding: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 10), size: .init(width: frame.width, height: 44))
        textField.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        line.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        line.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        line.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.90).isActive = true
        line.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
    }
}

extension CustomTextField: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text as NSString? else { return true }
        let updateText = text.replacingCharacters(in: range, with: string)
        print(updateText)
        completionText?(updateText)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.returnKeyType = .next
        textField.addTarget(self, action: #selector(UIResponder.resignFirstResponder), for: .editingDidEndOnExit)
        return true
    }
}
