//
//  AppViewController.swift
//  SampleRxSwift
//
//  Created by Yan Cervantes on 2/18/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import UIKit
import RxSwift

class AppViewController: UIViewController {
    let disposebag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc func handleBack() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func handleDismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    func customButton(title: String, backgroundColor: UIColor, selector: Selector) -> UIButton {
        let button = UIButton(type: .system)
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 7
        button.backgroundColor = backgroundColor
        button.addTarget(self, action: selector, for: .touchUpInside)
        return button
    }
    
    func customLabel(title: String, font: UIFont, color: UIColor) -> UILabel {
        let label = UILabel()
        label.text = title
        label.font = font
        label.textColor = color
        label.backgroundColor = UIColor.white
        return label
    }
    
}
