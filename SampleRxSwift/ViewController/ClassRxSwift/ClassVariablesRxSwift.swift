//
//  ClassVariablesRxSwift.swift
//  SampleRxSwift
//
//  Created by Yan Cervantes on 2/19/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ClassVariablesRxSwift: AppViewController {
    
    let names = BehaviorRelay(value: ["Yan"])
    let myModel = BehaviorRelay(value: [MyModel]())
    let boolValue = BehaviorRelay(value: false)
    var timer = Timer()
    
    let namesLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        label.numberOfLines = 0
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        observableVariable()
        timer(3.0)
        filteringMapingCollelction()
        behaviorValue()
    }
    
    private func setupViews() {
        view.addSubview(namesLabel)
        namesLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 25, left: 10, bottom: 0, right: 10))
    }
    
    private func behaviorValue() {
        myModel.asObservable().subscribe(onNext: { (model) in
           print(model)
        }).disposed(by: disposebag)
        
        let new = MyModel(title: "One", id: 1)
        let two = MyModel(title: "two", id: 2)
        timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: false, block: { [unowned self] (_) in
            self.myModel.accept([new, two])
            _ = self.myModel.value.map({ (model)  in
                print(model.title)
                print(model.id)
            })
        })
    }
    
    private func timer(_ time: TimeInterval) {
        timer = Timer.scheduledTimer(withTimeInterval: time, repeats: false, block: { [unowned self] (_) in
            self.names.accept(["Yan", "Aide", "Omar"])
            self.timer.invalidate()
        })
    }
    
    private func observableVariable() {
        names.asObservable().subscribe(onNext: { [unowned self] (value) in
            self.namesLabel.text = value.last
            print(value)
        }).disposed(by: disposebag)
    }
    
    // concatenate filtering and map in one function...
    private func filteringMapingCollelction() {
        names.asObservable().throttle(0.5, scheduler: MainScheduler.instance).filter { value in
            return value.count > 1
            }.map { value in
                return " values: \(value)"
            }.subscribe(onNext: { value in
                print("final job subscribe after filter and map values:", value)
            }).disposed(by: disposebag)
    }
    
    static func push(navigation: UINavigationController) {
        let view = ClassVariablesRxSwift()
        navigation.pushViewController(view, animated: false)
    }
}
