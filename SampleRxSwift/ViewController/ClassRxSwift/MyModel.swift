//
//  MyModel.swift
//  SampleRxSwift
//
//  Created by Yan Cervantes on 2/20/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import Foundation

struct MyModel {
    var title: String
    var id: Int
}
