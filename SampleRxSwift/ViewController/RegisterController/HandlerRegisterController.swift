//
//  HandlerRegisterController.swift
//  SampleRxSwift
//
//  Created by Adrian Cervantes on 4/26/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import Foundation

extension RegisterController {

    func setupConstraints() {
        nameTextField.setupValues(placeholder: "Name", image: #imageLiteral(resourceName: "man-user"))
        nameTextField.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 20, left: 5, bottom: 0, right: 5), size: .init(width: 0, height: 60))
        
        lastNameTextField.setupValues(placeholder: "Last Name", image: #imageLiteral(resourceName: "man-user"))
        lastNameTextField.anchor(top: nameTextField.safeAreaLayoutGuide.bottomAnchor, leading: nameTextField.leadingAnchor, bottom: nil, trailing: nameTextField.trailingAnchor, padding: .init(top: 15, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 60))
        
        mailTextFiel.setupValues(placeholder: "Email", image: #imageLiteral(resourceName: "envelope"))
        mailTextFiel.anchor(top: lastNameTextField.safeAreaLayoutGuide.bottomAnchor, leading: lastNameTextField.leadingAnchor, bottom: nil, trailing: lastNameTextField.trailingAnchor, padding: .init(top: 15, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 60))
        
        dateTextField.setupValues(placeholder: "Date of Birht", image: #imageLiteral(resourceName: "calendar"))
        dateTextField.anchor(top: mailTextFiel.safeAreaLayoutGuide.bottomAnchor, leading: mailTextFiel.leadingAnchor, bottom: nil, trailing: mailTextFiel.trailingAnchor, padding: .init(top: 15, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 60))
        
        registerButton.anchor(top: nil, leading: view.leadingAnchor, bottom: cancelButton.topAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 25, bottom: 20, right: 25), size: .init(width: 0, height: 44))
        cancelButton.anchor(top: nil, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 25, bottom: 20, right: 25), size: .init(width: 0, height: 44))
    }
}
