//
//  RegisterController.swift
//  SampleRxSwift
//
//  Created by Adrian Cervantes on 4/26/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import UIKit

class RegisterController: AppViewController {
    var registerValues = [String: Any]()
    
    var nameTextField = CustomTextField()
    var lastNameTextField = CustomTextField()
    var mailTextFiel = CustomTextField()
    var dateTextField = CustomTextField()
    lazy var registerButton = customButton(title: "Register", backgroundColor: .purple, selector: #selector(handleRegister))
    lazy var cancelButton = customButton(title: "cancel", backgroundColor: .purple, selector: #selector(handleDismiss))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupNavigationItems()
        setupViews()
    }
    
    private func setupViews() {
        view.addSubviews(views: nameTextField, lastNameTextField, mailTextFiel, dateTextField, registerButton, cancelButton)
        setupConstraints()
        textData()
    }
    
    @objc private func handleRegister() {
        let alert = CustomAlert()
        alert.completionAlert = {
            print("do stuff")
        }
        alert.show(animated: true, title: "Some Title", description: "Some demo for action alert, with large description to set messages or more")
    }
    
    private func textData() {
        nameTextField.completionText = { [weak self] (nameText) in
            self?.registerValues.updateValue(nameText, forKey: "user_name")
        }
        lastNameTextField.completionText = { [weak self] (lastNameText) in
            self?.registerValues.updateValue(lastNameText, forKey: "user_last_name")
        }
        mailTextFiel.completionText = { [weak self] (mailText) in
            self?.registerValues.updateValue(mailText, forKey: "user_mail")
        }
        dateTextField.completionText = { [weak self] (dateText) in
            self?.registerValues.updateValue(dateText, forKey: "user_date_birht")
        }
    }
    
    private func setupNavigationItems() {
        let backButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(handleBack))
        navigationItem.leftBarButtonItem = backButton
    }
    
    static func push(view: UIViewController) {
        let register = RegisterController()
        view.present(register, animated: true, completion: nil)
    }
}
