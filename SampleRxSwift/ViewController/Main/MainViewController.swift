//
//  ViewController.swift
//  SampleRxSwift
//
//  Created by Yan Cervantes on 2/18/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import UIKit

class MainViewController: AppViewController {
    
    lazy var collectionButton = customButton(title: "Go to collection", backgroundColor: .purple, selector: #selector(handleGoToCollection))
    lazy var tableButton = customButton(title: "Go to table", backgroundColor: .purple, selector: #selector(handleGoToTable))

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        setupNavigationItems()
    }
    
    private func setupNavigationItems() {
        navigationItem.title = "Main View"
        let rightButton = UIBarButtonItem(title: "RxVariables", style: .plain, target: self, action: #selector(handleClassRxSwift))
        navigationItem.rightBarButtonItem = rightButton
        let lefButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(handleRegister))
        navigationItem.leftBarButtonItem = lefButton
    }
    
    @objc private func handleRegister() {
        RegisterController.push(view: self)
    }
    
    @objc private func handleClassRxSwift() {
        guard let navigation = navigationController else { return }
        ClassVariablesRxSwift.push(navigation: navigation)
    }
    
    @objc private func handleGoToCollection() {
        guard let navigation = navigationController else { return }
        MyCollectionViewController.push(navigation: navigation)
    }
    
    @objc private func handleGoToTable() {
        guard let navigation = navigationController else { return }
        MyTableViewController.push(navigation: navigation)
    }
    
    private func setupViews() {
        view.addSubview(collectionButton)
        view.addSubview(tableButton)
        collectionButton.anchor(top: nil, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 30, bottom: 15, right: 30), size: CGSize(width: 0, height: 44))
        tableButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 15, left: 30, bottom: 0, right: 30), size: CGSize(width: 0, height: 44))
    }
    
}

