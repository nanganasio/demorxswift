//
//  CollectionCell.swift
//  SampleRxSwift
//
//  Created by Yan Cervantes on 2/18/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {
    
    let imageService = ImageService()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.backgroundColor = .clear
        return label
    }()
    
    let imageCourse: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        imageView.backgroundColor = .clear
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(titleLabel)
        addSubview(imageCourse)
        
        imageCourse.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 20), size: .init(width: 80, height: 80))
        imageCourse.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        titleLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 30, bottom: 0, right: 25))
        titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell(model: Model) {
        titleLabel.text = model.name
        guard let url = model.imageUrl else { return }
        print(model)
        imageService.downloadImage(withBucketURL: url) { [weak self] (result) in
            switch result {
            case .failure(let error):
                print("error", error.localizedDescription)
            case .success(let image):
                self?.imageCourse.image = image
            }
        }
    }
    static var identifier: String {
        return String(describing: self)
    }
}
