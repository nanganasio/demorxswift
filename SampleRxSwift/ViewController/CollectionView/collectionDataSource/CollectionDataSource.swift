//
//  CollectionDataSource.swift
//  SampleRxSwift
//
//  Created by Yan Cervantes on 2/18/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension MyCollectionViewController: UICollectionViewDelegateFlowLayout {
    func bindCollectionView() {
        myCollection.register(CollectionCell.self, forCellWithReuseIdentifier: CollectionCell.identifier)
        
        self.modelImage.bind(to: self.myCollection.rx.items(cellIdentifier: CollectionCell.identifier, cellType: CollectionCell.self)) {_, element, cell in
            cell.setupCell(model: element)
            }.disposed(by: disposebag)

        //     MARK: can be seen the values on the collection but when is updating, cannot... use up func to updates values every values change
        
//        Observable.of(collectionList.value).bind(to: myCollection.rx.items(cellIdentifier: CollectionCell.identifier, cellType: CollectionCell.self)) { (_, element, cell) in
//            cell.titleLabel.text = element
//        }.disposed(by: disposebag)
        
        myCollection.rx.itemSelected.subscribe(onNext: { [unowned self] indexpath in
            print(self.modelImage.value[indexpath.row].name ?? "")
        }).disposed(by: disposebag)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 100)
    }
}
