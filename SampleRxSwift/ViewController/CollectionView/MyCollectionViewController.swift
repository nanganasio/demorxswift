//
//  MyCollectionViewController.swift
//  SampleRxSwift
//
//  Created by Yan Cervantes on 2/18/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MyCollectionViewController: AppViewController {
    
    var modelImage = BehaviorRelay(value: [Model]())
    
    lazy var myCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = .white
        return collection
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        bindCollectionView()
        getModel { [weak self] (result) in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
            case .success(let data):
                self?.modelImage.accept(data ?? [])
            }
        }
    }
    
    private func setupViews() {
        myCollection.delegate = self
        view.addSubview(myCollection)
        myCollection.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor)
    }
    
    private func getModel(completion: @escaping ( Result<[Model]?, Error>) -> Void) {
        let urlString = "https://api.letsbuildthatapp.com/jsondecodable/courses"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
            }
            guard let data = data else { return }
            do {
                let model = try JSONDecoder().decode([Model].self, from: data)
                completion(.success(model))
            } catch let error {
                completion(.failure(error))
            }
        }.resume()
    }

    static func push(navigation: UINavigationController) {
        let view = MyCollectionViewController()
        navigation.pushViewController(view, animated: true)
    }
    
}
