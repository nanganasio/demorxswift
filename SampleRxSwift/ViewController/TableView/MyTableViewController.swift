//
//  MyTableViewController.swift
//  SampleRxSwift
//
//  Created by Yan Cervantes on 2/18/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MyTableViewController: AppViewController {
    var list = BehaviorRelay(value: ["Omar", "Erwin", "Chavira", "Buck" , "Yan", "Jefe", "Jefa"])
    
    var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        bindTableView()
        setupNavigationItems()
    }
    
    private func setupViews() {
        myTableView = UITableView(frame: .zero, style: .plain)
        myTableView.separatorStyle = .none
        myTableView.delegate = self
        view.addSubview(myTableView)
        myTableView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor)
    }
    
    private func setupNavigationItems() {
        let addNew = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(handleNew))
        navigationItem.rightBarButtonItem = addNew
    }
    
    @objc private func handleNew() {
        let alert = CustomAlert()
        alert.completionAlert = { [weak self] in
            let new = "Yan"
            self?.list.acceptAppendingIndex(element: new, index: 0)
          //  self?.list.acceptAppending(element: new)
        }
        alert.show(animated: true, title: "Agregar", description: "se agregará un item")
    }
    static func push(navigation: UINavigationController) {
        let view = MyTableViewController()
        navigation.pushViewController(view, animated: true)
    }
}
