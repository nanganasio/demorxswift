//
//  MyTableCell.swift
//  SampleRxSwift
//
//  Created by Adrian Cervantes on 5/3/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import UIKit

class MyTableCell: UITableViewCell {
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.backgroundColor = .clear
        return label
    }()
    
    lazy var deleteButton: UIButton = {
       let button = UIButton(type: .system)
        button.setTitle(" - ", for: .normal)
        button.backgroundColor = .red
        button.layer.cornerRadius = button.frame.height / 2
        button.addTarget(self, action: #selector(handleDelete), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    var completionCell: ( () -> Void)?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func handleDelete() {
        let alert = CustomAlert()
        alert.completionAlert = { [weak self] in
            self?.completionCell?()
        }
        alert.show(animated: true, title: "Borrar", description: "se borrara el item seleccionado")
    }
    
    func setupCell(values: String) {
        nameLabel.text = values
    }
    
    private func setupViews() {
        addSubviews(views: nameLabel, deleteButton)
        nameLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 15, bottom: 0, right: 15))
        nameLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        deleteButton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,
                            padding: .init(top: 0, left: 0, bottom: 0, right: 15),
                            size: .init(width: 50, height: 50))
        deleteButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    static var identifier: String {
        return String(describing: self)
    }
}
