//
//  TableDataSource.swift
//  SampleRxSwift
//
//  Created by Yan Cervantes on 2/18/19.
//  Copyright © 2019 Yan Cervantes. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension MyTableViewController: UITableViewDelegate {
    func bindTableView() {
        myTableView.register(MyTableCell.self, forCellReuseIdentifier: MyTableCell.identifier)
        
        list.bind(to: myTableView.rx.items(cellIdentifier: MyTableCell.identifier, cellType: MyTableCell.self)) { (index, element, cell) in
            cell.setupCell(values: element)
            cell.completionCell = { [weak self] in
                self?.list.remove(index: index)
            }
        }.disposed(by: disposebag)
       
        // MARK: use the upper function to get data updating and changing when list is update
        
//        Observable.of(list.value).bind(to: myTableView.rx.items(cellIdentifier: "cellId", cellType: UITableViewCell.self)) { (_, element, cell) in
//            cell.textLabel?.text = element
//        }.disposed(by: disposebag)
    
        myTableView.rx.itemSelected.subscribe(onNext: { [weak self] indexpath in
            self?.myTableView.deselectRow(at: indexpath, animated: false)
            print(self?.list.value[indexpath.row] ?? "")
        }).disposed(by: disposebag)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
}
